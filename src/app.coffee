# libs
Vue = require('vue')

# components
app = require('./app.vue')

# directives
infiniteScroll = require('./directives/infinite-scroll.coffee')

# start new Vuejs app
vm = new Vue(
    el: 'body'

    components:
        app: app

)

Vue = require('vue')
jQuery = $ = require('jquery')

infiniteScroll = Vue.directive('infinite-scroll',
    acceptStatement: true,

    templates:
        loading: '<div class="loading container text-center">
                      <div class="row">
                          <div class="col-xs-12">
                              <img src="/images/loading.gif" width=50 height=50>
                          </div>
                      </div>
                  </div>'
        completed: '<div class="completed container text-center">
                        <div class="row">
                            <div class="col-xs-12">
                                <p>~ End of Catalogue ~</p>
                            </div>
                        </div>
                    </div>'

    params: ['unbind']

    bind: ->
        # data to pass to handler
        data =
            self: @

        # bind scrolling, resizing events
        $(window).on 'DOMContentLoaded load resize scroll', data, @handler

    unbind: ->
        # append complete template
        @appendTemplate 'completed'

        # unbind scrolling, resizing events
        $(window).off 'DOMContentLoaded load resize scroll'

    update: ->
        # this
        self = @
        # current component
        @component = @el.__vue__

        # append/remove loading depending of the component's ready event
        @component.$on 'ready', (isReady) ->
            # set directive param to components'
            self.params.isReady = isReady

            # append/remove loading
            if isReady then self.removeTemplate 'loading' else self.appendTemplate 'loading'

    handler: (e) ->
        # fetch data passed to handler
        self = e.data.self

        # call component method if at scrolled to bottom
        self.callMethod() if self.isBottom()

    callMethod: ->
        # unbind if complete else call directive expression
        if @isComplete()
            @unbind()
        else if @isReady()
            @component.$eval(@expression)

    appendTemplate: (template) ->
        # append requested template to element
        $(@el).append @templates[template]

    removeTemplate: (template) ->
        # prepend a dot to template for element class
        template = ".#{template}"

        # remove requested template from element
        $(@el).find template
              .remove()

    isComplete: ->
        # call component complete method if check-complete param has been passed else false
        @component.$eval(@params.unbind) if @params.unbind?

    isReady: ->
        # check if is ready
        not @params.isReady? or @params.isReady

    isBottom: ->
        # get window element
        $window = $(window)
        # get document element
        $document = $(document)
        # get element bounds
        el = @el.getBoundingClientRect()

        # check element bottom to window or document bottom
        el.bottom == ($window.innerHeight() or $document.innerHeight())
)

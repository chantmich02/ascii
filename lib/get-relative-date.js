function getSecond (s) {
    return s * 1000;
}

function getMinute (m) {
    return m * getSecond(60);
}

function getHour (h) {
    return h * getMinute(60);
}

function getDay (d) {
    return d * getHour(24);
}

function getWeek (w) {
    return w * getDay(7);
}

function getDateDifference(date) {
    var now = new Date();

    return now.getTime() - date.getTime();
}

function getMonthName (i, type) {
    // month name
    var monthNames = {
        short: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        long: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    };

    // check if type is define in monthName object otherwise default to 'short'
    type = typeof monthNames[type] === 'undefined' ||  monthNames[type] === null ? 'short' : type;

    return monthNames[type][i % 12];
}

function toFullDate (date, type) {
    return date.getDate() + ' ' + getMonthName(date.getMonth(), type) + ' ' + date.getFullYear();
}

function toSeconds (ms) {
    return Math.floor(ms / 1000);
}

function toMinutes (ms) {
    return Math.floor(ms / getSecond(60));
}

function toHours (ms) {
    return Math.floor(ms / getMinute(60));
}

function toDays (ms) {
    return Math.floor(ms / getHour(24));
}

function toWeeks (ms) {
    return Math.floor(ms / getDay(7));
}

function isToday (ms) {
    return toDays(ms) <= 1;
}

function isYesterday (ms) {
    var d = toDays(ms);

    return  d > 1 && d <= 2;
}

function isAWeek (ms) {
    return toWeeks(ms) >= 1;
}

function isDate (date) {
    return date instanceof Date;
}

module.exports = function (date, type) {
    // make sure date is a Date instance
    date = isDate(date) ? date : new Date(date);
    type = type || 'short';

    // get the difference between now and the date in milliseconds
    var diff = getDateDifference(date)

    // a week or more return formatted date
    if (isAWeek(diff)) {
        return toFullDate(date, type);
    }

    // check and convert difference to relative time
    if (isToday(diff)) {
        // less than 1 day ago
        return 'Today';
    }
    else if (isYesterday(diff)) {
        // between 1 day and 2 days ago
        return 'Yesterday';
    }

    // number of days ago
    return toDays(diff) + ' days ago';
}

function toDollars (c) {
    var dollars = c / 100

    return '$' + dollars.toFixed(2);
}

module.exports = function (cents) {
    return toDollars(cents);
}

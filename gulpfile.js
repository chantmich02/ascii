var elixir = require('laravel-elixir');
var vueify = require('laravel-elixir-vueify');
var config = elixir.config;

// modify elixir default paths
config.assetsPath = './src/assets';
config.publicPath = './static';
config.publicJs = config.publicPath + '/' + config.js.outputFolder

// add browserify transformer
config.js.browserify.transformers.push({
    name: 'coffeeify',
    options: {}
});

// tasks
elixir(function(mix) {
    mix.sass('app.scss')
       .browserify('../../app.coffee')
       .copy('./node_modules/bootstrap/dist/js/bootstrap.min.js', config.publicJs)
       .copy('./node_modules/jquery/dist/jquery.min.js', config.publicJs)
       .copy('./node_modules/vue/dist/vue.min.js', config.publicJs)
});
